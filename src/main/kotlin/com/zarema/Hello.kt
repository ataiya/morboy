package com.zarema

val firstUser = User(true)
val secondUser = User(false)

fun main(args: Array<String>) {
    println("Hello, World")

    println("Установка кораблей первого игрока")
    while (firstUser.myBoard.getSizesOfShips().isNotEmpty()){
        println("Вы можете установить следующие корабли: ")
        firstUser.myBoard.getSizesOfShips().forEach { i ->
            print(" $i")
        }
        println("\nВведите размер корабля")
        val size = readLine()
        if(size != null && size.toInt() in (1..4)){
            println("Введите координаты клетки и направление")
            val(inputX, inputY, inputDir) = readLine()!!.split(' ')

            if (inputDir.toCharArray()[0].toUpperCase() != DIRECTION_DOWN && inputDir.toCharArray()[0].toUpperCase() != DIRECTION_RIGHT){
                println("Неверное направление")
                continue
            }

            if (inputX != null && inputY != null) {
                firstUser.shipPlace(size.toInt(),
                    Cell(inputX.toInt(), inputY.toInt()),
                    inputDir.toCharArray()[0].toUpperCase())
                firstUser.myBoard.draw()
            }
        } else {
            println("Неверный размер")
        }
    }


    println("\n\n\n\n\n\nУстановка кораблей второго игрока")
    while (secondUser.myBoard.getSizesOfShips().isNotEmpty()){
        println("Вы можете установить следующие корабли: ")
        secondUser.myBoard.getSizesOfShips().forEach { i ->
            print(" $i")
        }
        println("\nВведите размер корабля")
        val size = readLine()
        if(size != null && size.toInt() in (1..4)){
            println("Введите координаты клетки и направление")
            val(inputX, inputY, inputDir) = readLine()!!.split(' ')

            if (inputDir.toCharArray()[0].toUpperCase() != DIRECTION_DOWN && inputDir.toCharArray()[0].toUpperCase() != DIRECTION_RIGHT){
                println("Неверное направление")
                continue
            }

            if (inputX != null && inputY != null) {
                secondUser.shipPlace(size.toInt(),
                    Cell(inputX.toInt(), inputY.toInt()),
                    inputDir.toCharArray()[0].toUpperCase())
                secondUser.myBoard.draw()
            }
        } else {
            println("Неверный размер")
        }
    }

    firstUser.isMyTurn = true
    println("\n\n\n\n\n\nУстановка кораблей Завершена")

    var user: User

    // Атакуем пока есть живые корабли
    while (firstUser.myBoard.getSizesOfAliveShips().isNotEmpty() &&
        secondUser.myBoard.getSizesOfAliveShips().isNotEmpty()) {

        user = if(firstUser.isMyTurn){
            firstUser
        } else {
            secondUser
        }
        println("\n\n\n\n\n\nМОЯ ДОСКА")
        user.myBoard.draw()

        println("ДОСКА СОПЕРНИКА")
        user.enemyBoard.draw()

        println("Введите клетку атаки")
        val(inputX, inputY) = readLine()!!.split(' ')
        if (inputX != null && inputY != null) {
            user.attack(Cell(inputX.toInt(), inputY.toInt()))
        }
    }

    println("\nИГРА ЗАКОНЧЕНА")
    if (firstUser.myBoard.getSizesOfAliveShips().isEmpty()){
        println("ПОБЕДИЛ ВТОРОЙ ИГРОК")
    }else{
        println("ПОБЕДИЛ ПЕРВЫЙ ИГРОК")
    }
}

