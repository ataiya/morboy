package com.zarema

class User(var isFirst: Boolean) {
    var myBoard: Board = Board()
    var enemyBoard: Board = Board()
    var isReady: Boolean = false
    var isMyTurn: Boolean = false

    // Даем нужным клеткам статус Шиппед и меняем статус коробля на живого
    private fun setBoardCells(ship: Ship){
        for(shipCellId in ship.cells_id){
            this.myBoard.setCellStatus(shipCellId, CELL_ST_SHIPPED)
        }

        ship.status = SHIP_ST_ALIVE
    }

    // Установка кораблей и соответствующих клеток на поле
    fun shipPlace(size: Int, cell: Cell, direction: Char): Boolean {
        // Проверяем не вышли ли за границы
        if ((direction == DIRECTION_RIGHT && (cell.x + size - 1) > BOARD_X_MAX) ||
            (direction == DIRECTION_DOWN && (cell.y + size - 1) > BOARD_Y_MAX)){
            return false
        }

        // Создаем тестовый корабль и проверяем на пересечение
        var testShip = Ship(size)
        testShip.setShipCells(cell, direction)
        if(this.myBoard.intersectChecking(testShip)) return false

        // Данная провека не позволит создать лишнее кол-во кораблей
        if (size == 1) {
            (0 until 4).forEach { id ->
                if (this.myBoard.ships[id].setShipCells(cell, direction)) {
                    setBoardCells(this.myBoard.ships[id])
                    return true
                }
            }
        }
        else if (size == 2) {
            (4 until 7).forEach { id ->
                if (this.myBoard.ships[id].setShipCells(cell, direction)) {
                    setBoardCells(this.myBoard.ships[id])
                    return true
                }
            }
        }
        else if (size == 3) {
            (7 until 9).forEach { id ->
                if (this.myBoard.ships[id].setShipCells(cell, direction)) {
                    setBoardCells(this.myBoard.ships[id])
                    return true
                }
            }
        }
        else if (size == 4) {
            if(this.myBoard.ships[9].setShipCells(cell, direction)) {
                setBoardCells(this.myBoard.ships[9])
                return true
            }
        }

        return false
    }

    // Функция атаки заданной клетки
    fun attack(attackCell: Cell): Boolean {
        var enemy = firstUser
        if(this.isFirst){
            enemy = secondUser
        }

        // Если атака была за границей, то сохраняем право хода
        if(attackCell.x < BOARD_X_MIN ||
            attackCell.x > BOARD_X_MAX ||
            attackCell.y < BOARD_Y_MIN ||
            attackCell.y > BOARD_Y_MAX){
            this.isMyTurn = true
            enemy.isMyTurn = false

            return false
        }

        // Получаем айди клетки по которой атакуют
        val attackCellId = (attackCell.y - 1) * BOARD_Y_MAX + attackCell.x -1

        // Если попали, то сохраняем право хода
        if(enemy.myBoard.getCellStatus(attackCell.x, attackCell.y) == CELL_ST_SHIPPED){
            enemy.myBoard.setCellStatus(attackCellId, CELL_ST_FIRED)
            this.enemyBoard.setCellStatus(attackCellId, CELL_ST_FIRED)
            this.isMyTurn = true
            enemy.isMyTurn = false
        } else {
            //Если промахнулись, меняем право хода
            enemy.myBoard.setCellStatus(attackCellId, CELL_ST_MISSED)
            this.enemyBoard.setCellStatus(attackCellId, CELL_ST_MISSED)
            this.isMyTurn = false
            enemy.isMyTurn = true
        }

        return true
    }

}