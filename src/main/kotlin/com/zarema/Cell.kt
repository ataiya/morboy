package com.zarema

class Cell(var x: Int, var y: Int) {
    var status = CELL_ST_EMPTY
    val id: Int
        get() = (this.y - 1) * BOARD_Y_MAX + (this.x - 1)
}
