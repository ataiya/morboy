package com.zarema

class Board {
    private var cells = arrayListOf<Cell>()
    var ships = arrayListOf<Ship>()

    // Создаем пустую доску
    private fun initBoard(){
        for (y in 1..BOARD_Y_MAX) for (x in 1..BOARD_X_MAX)
            cells.add(Cell(x,y))
    }

    // Создаем нужные корабли
    private fun initShips(){
        for(i in 1..10){
            if (i == 10) {
                //4 мачтовый
                this.ships.add(Ship(4))
            }
            else if (i >= 8) {
                //3 мачтовый
                this.ships.add(Ship(3))
            }
            else if (i >= 5) {
                //2 мачтовый
                this.ships.add(Ship(2))
            }
            else {
                //1 мачтовый
                this.ships.add(Ship(1))
            }
        }
    }


    // Проверка на пересечение или соседство с существующими кораблями
    fun intersectChecking(testShip: Ship): Boolean {
        testShip.cells_id.forEach { testCellId ->
            this.cells.forEach { cell ->
                if (cell.status == CELL_ST_SHIPPED) {
                    if (kotlin.math.abs(cell.x - this.cells[testCellId].x) <= 1 &&
                        kotlin.math.abs(cell.y - this.cells[testCellId].y) <= 1) {
                        return true
                    }
                }
            }
        }

        return false
    }

    // Получение значения статуса заданной клетки
    fun getCellStatus(x: Int, y: Int):Char{
        // минус один так как клетки не с нуля
        return this.cells[((y-1) * BOARD_Y_MAX) + (x-1)].status
    }


    // Установка значения статуса заданной клетки
    fun setCellStatus(cellId: Int, cellStatus: Char){
        this.cells[cellId].status = cellStatus
    }


    // Если хотя бы одна клетка жива, то и корабль жив
    private fun deathChecking(ship: Ship): Boolean {
        ship.cells_id.forEach { cellId ->
            if(this.cells[cellId].status != CELL_ST_FIRED &&
                this.cells[cellId].status != CELL_ST_DEAD){
                return false
            }
        }

        ship.cells_id.forEach { cellId ->
            this.cells[cellId].status = CELL_ST_DEAD
        }

        ship.status = SHIP_ST_DEAD

        return true
    }


    // Получаем размеры кораблей, которые можно установить
    fun getSizesOfShips(): ArrayList<Int> {
        var availableShips = arrayListOf<Int>()

        for(ship in this.ships){
            if (ship.status == SHIP_ST_NOT_PLACED){
                availableShips.add(ship.size)
            }
        }

        return availableShips
    }


    // Получаем размеры живых кораблей
    fun getSizesOfAliveShips(): ArrayList<Int>{
        var aliveShips = arrayListOf<Int>()
        for(ship in this.ships){
            if (!this.deathChecking(ship)){
                aliveShips.add(ship.size)
            }
        }

        return aliveShips
    }

    fun draw(){
        for (i in 0..BOARD_X_MAX){
            print(" $i")
        }
        print("\n")
        for (y in 1..BOARD_Y_MAX){
            print(" $y")
            for (x in 1..BOARD_X_MAX){
                print(" ${this.cells[(y-1) * BOARD_Y_MAX + x - 1].status}")
            }
            print("\n")
        }
    }

    init {
        initBoard()
        initShips()
    }
}