package com.zarema

const val CELL_ST_EMPTY = '*'
const val CELL_ST_MISSED = 'M'
const val CELL_ST_FIRED = 'X'
const val CELL_ST_DEAD = 'D'
const val CELL_ST_SHIPPED = 'S'

const val DIRECTION_RIGHT = 'R'
const val DIRECTION_DOWN = 'D'

const val SHIP_ST_ALIVE = "Jiv"
const val SHIP_ST_DEAD = "Mertv"
const val SHIP_ST_NOT_PLACED = "Ne postavlen"

const val BOARD_X_MIN = 1
const val BOARD_X_MAX = 10
const val BOARD_Y_MIN = 1
const val BOARD_Y_MAX = 10

const val COMMAND_ATTACK = "fire"
