package com.zarema

class Ship(val size: Int) {
    var cells_id = arrayListOf<Int>()
    var status = SHIP_ST_NOT_PLACED

    // При создании даем каждой клетке корабля айди -1
    private fun initShipCells(){
        (1..this.size).forEach { _ ->
            this.cells_id.add(-1)
        }
    }

    // Если хотя бы одна клетка имеет айди -1, значит корабль не готов
    private fun readyChecking(): Boolean {
        this.cells_id.forEach {
            if(it == -1) {
                return false
            }
        }
        return true
    }

    // Даем координаты клеткам корабля и меняем статус
    fun setShipCells(cell: Cell, direction: Char): Boolean {
        // Если корабль не готов, то заходим внутрь
        if(!this.readyChecking()){
            var tempCell = Cell(cell.x, cell.y)
            if (direction == DIRECTION_DOWN) {
                for ( i in 0 until size){
                    cells_id[i] = tempCell.id
                    tempCell.y++
                }
            } else if (direction == DIRECTION_RIGHT) {
                for ( i in 0 until size){
                    cells_id[i] = tempCell.id
                    tempCell.x++
                }
            }

            this.status = SHIP_ST_ALIVE
            return true
        }

        // Если корабль уже был готов, то клетки не могут быть установлены
        return false
    }


    init {
        initShipCells()
    }
}